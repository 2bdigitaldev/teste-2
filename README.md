## REGRAS GERAIS
- O projeto deve ser feito com algum framework web como Vue.js, React.
- Você pode usar quaisquer bibliotecas de terceiros.
- Crie um README com as instruções para compilar, testar e rodar o projeto.

- O link do repositório deverá ser enviado para o e-mail debora.teixeira@totvs.com.br e claydson.silva@totvs.com.br com o título **[teste front-end]**


## Você deverá criar um SPA de previsão do tempo, onde requisitos são:
- Deve ser possível pesquisar a previsão do tempo para uma cidade.
- Exibir a previsão caso disponível.
- Tratar erro caso a previsão esteja indisponível, ou acontece algo inesperado.

Para conseguir realizar essa tarefa você deverá utilizar a popular api do OpenWeatherMap (http://openweathermap.org/). O mock das telas do que é desejavel que tenha no aplicativo, você pode dar um toque seu no design/layout, o importante é exibir ao menos a informação básica do que for retornado pelo OpenWeatherMap.

![home](assets/front-end-home.png)


## Seria desejável(não obrigatório)
- Utilizar algum padrão arquitetural
- Testes unitários


## O que iremos analisar:
- Semântica Web.
- Seu conhecimento geral sobre Front e API Rest.
- Como você organiza/estrutura seu código.
- Uso do Git.
- Sua habilidade de lidar com uma API REST.
- Sua habilidade de entender uma documentação.
- Clean code.
